# Webinfra access logs

Application to show access logs for DFS sites. This application will be soon phased out.

Use PHP s2i approach to deploy this application.

Then, create a secret under the Openshift project, and attach it to an environment variable under the DeploymentConfig.

Secret:

```yaml

kind: Secret
apiVersion: v1
metadata:
  labels:
    app: dfs-logs
  name: dfs-logs-credentials
  namespace: dfs-logs
data:
  CREDENTIALS_KEY: xxx
type: Opaque
```

DeploymentConfig:

```yaml
...
      containers:
        - env:
            - name: CREDENTIALS
              valueFrom:
                secretKeyRef:
                  key: CREDENTIALS_KEY
                  name: dfs-logs-credentials
...
```
