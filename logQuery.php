<?php

function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

$action = isset($_POST['action']) ? $_POST['action'] : null;
$sitename = isset($_POST['sitename']) ? $_POST['sitename'] : null;
$day = isset($_POST['day']) ? $_POST['day'] : null;
$size = isset($_POST['size']) ? $_POST['size'] : null;
$from = isset($_POST['from']) ? $_POST['from'] : null;
$sort = isset($_POST['sort']) ? $_POST['sort'] : null;

$credentials = getenv('CREDENTIALS');
$soapURL = 'https://webadminservices-iis7.web.cern.ch/webadminservices-iis7/webnamespace.asmx?wsdl';
$username = $_SERVER["ADFS_LOGIN"];
$usergroups = $_SERVER["ADFS_GROUP"];

$urlESBase = getenv('URL_ES_BASE');

if ($sitename) {
	$details = GetWebSiteDetails();
	if ($details) {
		if (IsUserAllowed($details)) {
			if ($action != NULL && $action !== false) {
				$siteType = $details->CentralDirectoryDetails->DirectoryType;
				// There's no error log for IIS, thus the not supported message
				if ($siteType == 'IIS8Dfs' && $action == 'getlogerror') {
					echo json_encode(array('status' => 'ERROR', 'message' => 'Operation not supported.'));
				} else {
					$result = getLogs($action, $day, $size, $from, $sort, $sitename, $siteType);
					echo json_encode(array('status' => 'OK', 'message' => $result));
				}
			} else {
				echo json_encode(array('status' => 'ERROR', 'message' => 'Operation not supported.'));
			}
		} else {
			echo json_encode(array('status' => 'ERROR', 'message' => 'You are not allowed to read this website\'s logs.'));
		}
	} else {
		echo json_encode(array('status' => 'ERROR', 'message' => 'Requested website does not exist.'));
	}
} else {
	echo json_encode(array('status' => 'ERROR', 'message' => 'Missing property: sitename.'));
}

function getLogs($action, $day, $size, $from, $sort, $sitename, $siteType) {
    $url = BuildEsApiUrl($action, $siteType, $day);
    $body = BuildEsApiBody($size, $from, $sort, $sitename, $siteType);
    $result = CallAPI('POST', $url, $body);
		
    return $result;
}

function BuildEsApiUrl($action, $siteType, $day) {
	global $urlESBase;

	return $urlESBase . 'iis_access_logs-' . $day .'/_search';
}

function BuildEsApiBody($size, $from, $sort, $sitename, $siteType) {
    $body = array();
    $body["from"] = $from;
    $body["size"] = $size;
    $body["sort"]["metadata.timestamp"] = $sort;
	
	switch($siteType) {
		case 'IIS8Dfs':
			$body["query"]["query_string"]["query"] = "data.cs_host:" . $sitename . ".web.cern.ch";
			break;
	}

    return $body;
}

function CallAPI($verb, $url, $data = null) {
	global $credentials;
    $ch = curl_init();

    $headers = array(
        'Content-Type: application/json'
    );

    if (($verb === 'PUT') || ($verb === 'POST')) {
        $dataStr = json_encode($data);
        $headers[] = 'Content-Length: ' . strlen($dataStr);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
    }

    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $credentials);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);

    curl_close($ch);

    return $result;
}

function GetWebsiteDetails() {
	global $soapURL, $sitename;
	
	// Prepare the SOAP call with required parameters
	$client = new soapclient($soapURL, array("exception" => 0)); 
	$request = array(
			array(
				"EntryName" => $sitename
			)
	);

	try {
		return $client->__soapcall('GetEntryDetails', $request)->GetEntryDetailsResult;
		// If error, capture it and return null
	} catch (SoapFault $fault) {
		return;
	}
}

function IsUserAllowed($details) {
	global  $username, $usergroups;

	$owner = $details->OwnerLogin;
	$allowed = $details->SiteModerators->string;
	
	// If only one moderator in the allowed list, convert the string into array for better handling
	if(!is_array($allowed)) {
		$allowed = array(
			0 => $allowed
		);
	}
	
	// Add the site owner to the allowed list
	array_push($allowed, $owner);
	
	// Add poweradmin groups to the allowed list
	array_push($allowed, "NICE WEB ADMINISTRATORS");
	array_push($allowed, "SERCO-ADMUS-ACCESS");
	array_push($allowed, "CERT-SEC");
	
	// If user in session is in the allowed list, return true
	if (in_array(strtoupper($username), $allowed)) {
		return true;
	}
	// Or if user in session is member of any group in the allowed list
	else {
		$groups = explode(';', $usergroups);
		foreach ($groups as $group) {
			if (in_array(strtoupper($group), $allowed)) {
				return true;
			}
		}
		return false;
	}
}

?>